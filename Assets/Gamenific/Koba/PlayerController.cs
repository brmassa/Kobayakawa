using UnityEngine;

namespace Gamenific.Koba
{
    internal class PlayerController : MonoBehaviour
    {
        [SerializeField]
        private PlayerComputerRandon PlayerAI = null;
        private Player player;

        #region MonoBehaviour
        private void OnDisable()
        {
            if (Player == null)
                return;

            Player.OnDrawStage -= _OnDrawStage;
            Player.OnBetStage -= _OnBetStage;

            Player.OnDrawChosen -= _OnDrawChosen;
            Player.OnGetCardChosen -= _OnDrawChosen;
            Player.OnBetChosen -= _OnBetChosen;
            Player.OnPassChosen -= _OnPassChosen;
            Player.OnCardChosen -= _OnCardChosen;
            Player.OnCardDiscarted -= _OnCardDiscarted;
            Player.OnCardGet -= _OnCardGet;
            Player.OnWeakestPlayer -= _OnWeakestPlayer;

            Player.OnWinRound -= _OnWinRound;
            Player.OnEliminated -= _OnEliminated;
            Player.OnBeingFirstPlayer -= _OnBeingFirstPlayer;
            Player.OnPlayerCurrent -= _OnPlayerCurrent;

            Player.OnTokensChanged -= _OnTokensChanged;
        }

        private void Start()
        {
            Player = Player;
        }
        #endregion


        #region Public Events
        public System.Action OnDrawStage { get; set; }
        public System.Action OnBetStage { get; set; }

        public System.Action OnDrawChosen { get; set; }
        public System.Action OnGetCardChosen { get; set; }
        public System.Action<int, int> OnBetChosen { get; set; }
        public System.Action OnPassChosen { get; set; }
        public System.Action<Card> OnCardChosen { get; set; }
        public System.Action<Card> OnCardDiscarted { get; set; }
        public System.Action<Card> OnCardGet { get; set; }
        public System.Action<Card> OnWeakestPlayer { get; set; }

        public System.Action<int, int> OnWinRound { get; set; }
        public System.Action OnEliminated { get; set; }
        public System.Action<bool> OnBeingFirstPlayer { get; set; }
        public System.Action<bool> OnPlayerCurrent { get; set; }

        public System.Action<int> OnTokensChanged { get; set; }
        #endregion

        #region Private Events
        private void _OnDrawStage() { if (OnDrawStage != null) OnDrawStage(); }

        private void _OnBetStage() { if (OnBetStage != null) OnBetStage(); }

        private void _OnDrawChosen() { if (OnDrawChosen != null) OnDrawChosen(); }

        private void _OnGetCardChosen() { if (OnGetCardChosen != null) OnGetCardChosen(); }

        private void _OnBetChosen(int value1, int value2) { if (OnBetChosen != null) OnBetChosen(value1, value2); }

        private void _OnPassChosen() { if (OnPassChosen != null) OnPassChosen(); }

        private void _OnCardChosen(Card card) { if (OnCardChosen != null) OnCardChosen(card); }

        private void _OnCardDiscarted(Card card) { if (OnCardDiscarted != null) OnCardDiscarted(card); }

        private void _OnCardGet(Card card) { if (OnCardGet != null) OnCardGet(card); }

        private void _OnWeakestPlayer(Card card) { if (OnWeakestPlayer != null) OnWeakestPlayer(card); }

        private void _OnWinRound(int value1, int value2) { if (OnWinRound != null) OnWinRound(value1, value2); }

        private void _OnEliminated() { if (OnEliminated != null) OnEliminated(); }

        private void _OnBeingFirstPlayer(bool isMe) { if (OnBeingFirstPlayer != null) OnBeingFirstPlayer(isMe); }

        private void _OnPlayerCurrent(bool isMe) { if (OnPlayerCurrent != null) OnPlayerCurrent(isMe); }

        private void _OnTokensChanged(int value) { if (OnTokensChanged != null) OnTokensChanged(value); }
        #endregion

        #region Public API
        public System.Action<Player> OnPlayerChanged { get; set; }

        public bool FirstPlayer { get { return Player.FirstPlayer; } }

        private Player Player
        {
            get { return player; }
            set
            {
                OnDisable();
                player = value;
                Hook();

                // Enable/disable the AI
                PlayerAI.enabled = (player.Type == Player.Types.Bot);

                if (OnPlayerChanged != null)
                    OnPlayerChanged(player);
            }
        }

        public bool Bet { get { return Player.Bet; } }

        public bool Eliminated { get { return Player.Eliminated; } }

        public Player.Types Type { get { return Player.Type; } }

        public void Init(Player player, int order)
        {
            Player = player;

            name = "Player " + order + " - " + Player.Type;
        }

        public Card Card { get { return Player.Card; } }
        public Card Card2 { get { return Player.Card2; } }
        public Card CardDiscarted { get { return Player.CardDiscarted; } }
        public bool CardSelected { get { return Player.CardSelected; } }
        public int Tokens { get { return Player.Tokens; } }

        [ContextMenu("DrawCardDecision")]
        public void DrawCardDecision()
        {
            Player.ChooseDraw();
        }

        [ContextMenu("GetCardDecision")]
        public void GetCardDecision()
        {
            Player.ChooseGet();
        }

        [ContextMenu("SelectCard1Decision")]
        public void SelectCard1Decision()
        {
            Player.ChooseCard();
        }

        [ContextMenu("SelectCard2Decision")]
        public void SelectCard2Decision()
        {
            Player.ChooseCard2();
        }

        [ContextMenu("BetDecision")]
        public void BetDecision()
        {
            Player.ChooseBet();
        }

        [ContextMenu("PassDecision")]
        public void PassDecision()
        {
            Player.ChoosePass();
        }
        #endregion

        private void Hook()
        {
            Player.OnDrawStage += _OnDrawStage;
            Player.OnBetStage += _OnBetStage;

            Player.OnDrawChosen += _OnDrawChosen;
            Player.OnGetCardChosen += _OnDrawChosen;
            Player.OnBetChosen += _OnBetChosen;
            Player.OnPassChosen += _OnPassChosen;
            Player.OnCardChosen += _OnCardChosen;
            Player.OnCardDiscarted += _OnCardDiscarted;
            Player.OnCardGet += _OnCardGet;
            Player.OnWeakestPlayer += _OnWeakestPlayer;

            Player.OnWinRound += _OnWinRound;
            Player.OnEliminated += _OnEliminated;
            Player.OnBeingFirstPlayer += _OnBeingFirstPlayer;
            Player.OnPlayerCurrent += _OnPlayerCurrent;

            Player.OnTokensChanged += _OnTokensChanged;
        }
    }
}
