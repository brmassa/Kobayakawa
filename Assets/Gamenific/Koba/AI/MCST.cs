﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class MCSTTreeNode
{
    private static System.Random r = new System.Random();
    private static int nActions = 5;
    private static double epsilon = 1e-6;
    private MCSTTreeNode[] children;
    private double nVisits, totValue;

    public void selectAction()
    {
        List<MCSTTreeNode> visited = new List<MCSTTreeNode>();
        MCSTTreeNode cur = this;
        visited.Add(this);
        while (!cur.isLeaf())
        {
            cur = cur.select();
            visited.Add(cur);
        }
        cur.expand();
        MCSTTreeNode newNode = cur.select();
        visited.Add(newNode);
        double value = rollOut(newNode);
        foreach (MCSTTreeNode node in visited)
        {
            // would need extra logic for n-player game
            node.updateStats(value);
        }
    }

    public void expand()
    {
        children = new MCSTTreeNode[nActions];
        for (int i = 0; i < nActions; i++)
        {
            children[i] = new MCSTTreeNode();
        }
    }

    private MCSTTreeNode select()
    {
        MCSTTreeNode selected = null;
        double bestValue = Double.MinValue;
        foreach (MCSTTreeNode c in children)
        {
            double uctValue = c.totValue / (c.nVisits + epsilon) +
                       Mathf.Sqrt((float)(Mathf.Log((float)nVisits + 1) / (c.nVisits + epsilon)))
                       + r.NextDouble() * epsilon;
            // small random number to break ties randomly in unexpanded nodes
            if (uctValue > bestValue)
            {
                selected = c;
                bestValue = uctValue;
            }
        }
        return selected;
    }

    public bool isLeaf()
    {
        return children == null;
    }

    public double rollOut(MCSTTreeNode tn)
    {
        // ultimately a roll out will end in some value
        // assume for now that it ends in a win or a loss
        // and just return this at random
        return r.Next(2);
    }

    public void updateStats(double value)
    {
        nVisits++;
        totValue += value;
    }

    public int arity()
    {
        return children == null ? 0 : children.Length;
    }
}
