﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Gamenific.Koba
{
    [Serializable]
    internal class NewGame
    {
        public int tokensPerPlayer = 4;
        [Serializable]
        public class Player
        {
            public Koba.Player.Types type = Koba.Player.Types.Human;
            public string name = "";
        }
        public List<Player> players = new List<Player>();
    }
}