﻿using UnityEngine;

namespace Gamenific.Koba
{
    internal class NewGameManager : MonoBehaviour
    {
        private static NewGameManager instance;
        public static NewGameManager Instance
        {
            get { return instance; }
        }

        [SerializeField]
        private int minPlayers = 3;

        [SerializeField]
        private int maxPlayers = 6;

        public NewGame game = new NewGame();

        #region MonoBehaviour
        private void Awake()
        {
            if (instance == null)
                instance = this;
            else
                Destroy(gameObject);
        }

        private void Start()
        {
            DontDestroyOnLoad(gameObject);
        }
        #endregion

        #region API
        public System.Action<NewGame.Player> OnAddPlayer { get; set; }
        public System.Action<NewGame.Player> OnRemovePlayer { get; set; }

        public int MinPlayers { get { return minPlayers; } }
        public int MaxPlayers { get { return maxPlayers; } }

        public void AddPlayer(Player.Types playerType = Player.Types.Bot)
        {
            if (game.players.Count < MaxPlayers)
            {
                var player = new NewGame.Player();
                player.type = playerType;
                game.players.Add(player);

                if (OnAddPlayer != null)
                    OnAddPlayer(player);
            }
        }

        public void RemovePlayer(NewGame.Player player)
        {
            if (game.players.Count > MinPlayers)
            {
                game.players.Remove(player);
                if (OnRemovePlayer != null)
                    OnRemovePlayer(player);
            }
        }

        public void Setup()
        {
            game.players.Clear();
            if (game.players.Count < MinPlayers)
            {
                int playersNeeded = MinPlayers - game.players.Count;
                for (int i = 0; i < playersNeeded; i++)
                    AddPlayer(i == 0 ? Player.Types.Human : Player.Types.Bot);
            }
        }
        #endregion
    }
}