﻿using UnityEngine;
using UnityEngine.UI;

namespace Gamenific.Koba.Menu
{
    internal class AddPlayerButton : MonoBehaviour
    {
        [SerializeField]
        private bool AddOperation = true;

        [SerializeField]
        private NewPlayer player = null;
        private Button button = null;

        public void Click()
        {
            if (AddOperation)
                NewGameManager.Instance.AddPlayer();
            else
                NewGameManager.Instance.RemovePlayer(player.Player);
        }

        #region MonoBehaviour
        private void Awake()
        {
            button = GetComponent<Button>();
        }

        private void Start()
        {
            NewGameManager.Instance.OnAddPlayer += EnableButton;
            NewGameManager.Instance.OnRemovePlayer += EnableButton;
        }

        private void OnDisable()
        {
            NewGameManager.Instance.OnAddPlayer -= EnableButton;
            NewGameManager.Instance.OnRemovePlayer -= EnableButton;
        }
        #endregion

        private void EnableButton(NewGame.Player player)
        {
            if (AddOperation)
                button.interactable = (NewGameManager.Instance.game.players.Count <= NewGameManager.Instance.MaxPlayers);
            else
                button.interactable = (NewGameManager.Instance.game.players.Count >= NewGameManager.Instance.MinPlayers);
        }
    }
}