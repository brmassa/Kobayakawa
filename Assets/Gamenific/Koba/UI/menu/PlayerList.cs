﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gamenific.Koba.Menu
{
    internal class PlayerList : MonoBehaviour
    {
        [SerializeField]
        private NewPlayer playerPrefab = null;

        #region MonoBehaviour
        private void OnDisable()
        {
            NewGameManager.Instance.OnAddPlayer -= CreatePlayer;
        }

        private void Start()
        {
            NewGameManager.Instance.OnAddPlayer += CreatePlayer;
            NewGameManager.Instance.Setup();
        }
        #endregion MonoBehaviour

        private void CreatePlayer(NewGame.Player playerData)
        {
            var player = Instantiate(playerPrefab, transform);
            player.Player = playerData;
        }
    }
}
