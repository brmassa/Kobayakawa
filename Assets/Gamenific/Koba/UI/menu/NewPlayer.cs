﻿using UnityEngine;
using UnityEngine.UI;

namespace Gamenific.Koba.Menu
{
    internal class NewPlayer : MonoBehaviour
    {
        [SerializeField] private Button m_RemoveButton = null;
        [SerializeField] private Image m_Icon = null;
        [SerializeField] private Sprite m_IconHuman = null;
        [SerializeField] private Sprite m_IconBot = null;
        private NewGame.Player player = null;

        #region MonoBehaviour
        private void OnEnable()
        {
            NewGameManager.Instance.OnRemovePlayer += OnRemovePlayer;
            NewGameManager.Instance.OnAddPlayer += OnAddPlayer;
        }

        private void OnDisable()
        {
            NewGameManager.Instance.OnRemovePlayer -= OnRemovePlayer;
            NewGameManager.Instance.OnAddPlayer -= OnAddPlayer;
        }
        #endregion

        public void ChangeType()
        {
            if (Player.type == Koba.Player.Types.Bot)
                Player.type = Koba.Player.Types.Human;
            else if (Player.type == Koba.Player.Types.Human)
                Player.type = Koba.Player.Types.Bot;
            ShowType();
        }

        public NewGame.Player Player
        {
            get { return player; }
            set
            {
                player = value;
                ShowType();
            }
        }

        private void OnRemovePlayer(NewGame.Player playerData)
        {
            m_RemoveButton.interactable = NewGameManager.Instance.game.players.Count > 3;
            if (playerData == player)
                Destroy(gameObject);
        }

        private void OnAddPlayer(NewGame.Player playerData)
        {
            m_RemoveButton.interactable = NewGameManager.Instance.game.players.Count > 3;
        }

        private void ShowType()
        {
            m_Icon.sprite = Player.type == Koba.Player.Types.Human ? m_IconHuman : m_IconBot;
        }
    }
}