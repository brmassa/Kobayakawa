using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gamenific.GodPlaysDice
{
    internal class LoadLevel : MonoBehaviour
    {
        public void LoadInt(int index)
        {
            SceneManager.LoadScene(index);
        }

        public void LoadString(string levelName)
        {
            SceneManager.LoadScene(levelName);
        }

        public void Quit()
        {
            Application.Quit();
        }
    }
}
