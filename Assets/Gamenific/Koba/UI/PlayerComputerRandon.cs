﻿using UnityEngine;
using System.Collections;

namespace Gamenific.Koba
{
    internal class PlayerComputerRandon : MonoBehaviour, IPlayerDecider
    {
        [SerializeField]
        private PlayerController player = null;

        [SerializeField]
        private float WaitTime = 1;
        private bool drawn = false;

        #region MonoBehaviour
        private void Awake()
        {
            if (player == null)
                player = GetComponent<PlayerController>();

            if (player == null)
                throw new UnityException("component needed");
        }

        private void OnEnable()
        {
            player.OnDrawStage += OnDrawStage;
            player.OnBetStage += OnBetStage;
            player.OnCardGet += OnCardGet;
        }

        private void OnDisable()
        {
            player.OnDrawStage -= OnDrawStage;
            player.OnBetStage -= OnBetStage;
            player.OnCardGet -= OnCardGet;
        }
        #endregion

        private void OnDrawStage()
        {
            StartCoroutine(DrawStageDecision());
        }

        public IEnumerator DrawStageDecision()
        {
            yield return new WaitForSeconds(WaitTime);

            if (Random.value > 0.5)
                player.DrawCardDecision();
            else
            {
                drawn = true;
                player.GetCardDecision();
            }
            yield return null;
        }

        private void OnBetStage()
        {
            StartCoroutine(BetStageDecision());
        }

        public IEnumerator BetStageDecision()
        {
            yield return new WaitForSeconds(WaitTime);

            if (Random.value > 0.5)
                player.BetDecision();
            else
                player.PassDecision();
            yield return null;
        }

        private void OnCardGet(Card card)
        {
            if (drawn)
            {
                drawn = false;
                StartCoroutine(CardSelectStageDecision());
            }
        }

        public IEnumerator CardSelectStageDecision()
        {
            yield return new WaitForSeconds(WaitTime);

            if (Random.value > 0.5)
                player.SelectCard1Decision();
            else
                player.SelectCard2Decision();
            yield return null;
        }
    }
}