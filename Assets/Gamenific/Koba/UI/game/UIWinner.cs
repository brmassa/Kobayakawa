﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Gamenific.Koba
{
    internal class UIWinner : MonoBehaviour
    {
        [SerializeField] private GameObject Panel = null;
        [SerializeField] private TextMeshProUGUI text = null;
        [SerializeField] private Button button = null;
        [SerializeField] private string m_WinnerNormal = "Player {0} won!";

        #region MonoBehaviour
        private void Awake()
        {
            if (Panel == null)
                throw new UnityException("component needed");
            if (text == null)
                throw new UnityException("component needed");
            if (button == null)
                throw new UnityException("component needed");
        }

        private void OnEnable()
        {
            GameManager.Instance.OnGameWin += DisplayWinner;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnGameWin -= DisplayWinner;
        }
        #endregion

        private void DisplayWinner(Player player)
        {
            Panel.SetActive(true);
            text.text = string.Format(m_WinnerNormal, player.PlayerNum + 1);
        }

        public void Click()
        {
            GameManager.Instance.SetupNewGame();
            Panel.SetActive(false);
            button.Select();
        }
    }
}