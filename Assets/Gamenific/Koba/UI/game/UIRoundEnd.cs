﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Gamenific.Koba
{
    internal class UIRoundEnd : MonoBehaviour
    {
        [SerializeField] private GameObject Panel = null;
        [SerializeField] private TextMeshProUGUI text = null;
        [SerializeField] private Button button = null;
        [SerializeField] private string m_WinnerNormal = "Player {0} won!";
        [SerializeField] private string m_WinnerOnly = " As only bidder";

        #region MonoBehaviour
        private void Awake()
        {
            if (Panel == null)
                throw new UnityException("component needed");
            if (text == null)
                throw new UnityException("component needed");
            if (button == null)
                throw new UnityException("component needed");
        }

        private void OnEnable()
        {
            GameManager.Instance.OnRoundPlayerWon += DisplayWinner;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnRoundPlayerWon -= DisplayWinner;
        }
        #endregion

        private void DisplayWinner(Player player, bool onlyBidder)
        {
            Panel.SetActive(true);
            text.text = string.Format(m_WinnerNormal, player.PlayerNum + 1) + (onlyBidder ? m_WinnerOnly : "");
        }

        public void Click()
        {
            GameManager.Instance.NextRound();
            Panel.SetActive(false);
            button.Select();
        }
    }
}