using UnityEngine;
using TMPro;

namespace Gamenific.Koba
{
    internal class UITokensInGame : MonoBehaviour
    {
        [SerializeField] private bool reserve = false;
        [SerializeField] private TextMeshProUGUI text = null;

        #region MonoBehaviour
        private void OnValidate()
        {
            if (text == null)
                text = GetComponent<TextMeshProUGUI>();
            if (text == null)
                throw new UnityException("Component missing");
        }

        private void OnEnable()
        {
            if (reserve)
                GameManager.Instance.OnTokensInReverse += TokensInReserve;
            else
                GameManager.Instance.OnTokensInGameRound += TokensInGameRound;
        }

        private void OnDisable()
        {
            if (reserve)
                GameManager.Instance.OnTokensInReverse -= TokensInReserve;
            else
                GameManager.Instance.OnTokensInGameRound -= TokensInGameRound;
        }
        #endregion

        private void TokensInGameRound(int tokens)
        {
            text.text = tokens.ToString();
        }

        private void TokensInReserve(int tokens)
        {
            text.text = tokens.ToString();
        }
    }
}
