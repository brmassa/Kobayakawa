using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Gamenific.Koba
{
    internal class UIButton : MonoBehaviour
    {
        [SerializeField] private Button button = null;
        [SerializeField] private TextMeshProUGUI cardText = null;
        [SerializeField] private Types type = Types.Bet;
        private Player player = null;

        private enum Types
        {
            BuyCard,
            DrawCard,
            Card1,
            Card2,
            Bet,
            Pass
        }

        #region MonoBehaviour
        private void Awake()
        {
            if (cardText == null)
            {
                if (button == null)
                    button = GetComponent<Button>();
                if (button == null)
                    throw new UnityException("Component missing");
            }
        }

        private void OnEnable()
        {
            GameManager.Instance.OnPlayerDrawStage += PlayerDrawStage;
            GameManager.Instance.OnPlayerBetStage += PlayerBetStage;
            GameManager.Instance.OnPlayerSelectStage += PlayerSelectStage;
            GameManager.Instance.OnPlayerDrawStage += SetEnableCard;
            GameManager.Instance.OnPlayerBetStage += SetEnableCard;
            GameManager.Instance.OnPlayerSelectStage += SetEnableCard;

            GameManager.Instance.OnStageChange += OnStageChange;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnPlayerDrawStage -= PlayerDrawStage;
            GameManager.Instance.OnPlayerBetStage -= PlayerBetStage;
            GameManager.Instance.OnPlayerSelectStage -= PlayerSelectStage;
            GameManager.Instance.OnPlayerDrawStage -= SetEnableCard;
            GameManager.Instance.OnPlayerBetStage -= SetEnableCard;
            GameManager.Instance.OnPlayerSelectStage -= SetEnableCard;

            GameManager.Instance.OnStageChange -= OnStageChange;
        }
        #endregion

        #region public
        public void Activate()
        {
            switch (type)
            {
                case Types.DrawCard:
                    player.ChooseDraw();
                    break;
                case Types.BuyCard:
                    player.ChooseGet();
                    break;

                case Types.Card1:
                    player.ChooseCard();
                    break;
                case Types.Card2:
                    player.ChooseCard2();
                    break;

                case Types.Bet:
                    player.ChooseBet();
                    break;
                case Types.Pass:
                    player.ChoosePass();
                    break;
            }
        }
        #endregion public

        private void PlayerDrawStage(Player player)
        {
            this.player = player;
            SetEnable(player.Type == Player.Types.Human && (type == Types.DrawCard || type == Types.BuyCard));
        }

        private void PlayerBetStage(Player player)
        {
            this.player = player;
            SetEnable(player.Type == Player.Types.Human && (type == Types.Bet || type == Types.Pass));
        }

        private void PlayerSelectStage(Player player)
        {
            SetEnable(player.Type == Player.Types.Human && (type == Types.Card1 || type == Types.Card2));
        }

        private void SetEnable(bool enable)
        {
            if (button != null)
                button.interactable = enable;
        }

        private void SetEnableCard(Player player)
        {
            var enable = player.Type == Player.Types.Human;
            if (cardText != null)
                if (type == Types.Card1)
                    cardText.text = enable && player.Card != null ? player.Card.Value.ToString() : "";
                else if (type == Types.Card2)
                    cardText.text = enable && player.Card2 != null ? player.Card2.Value.ToString() : "";
        }

        private void OnStageChange(Game.Stages stage)
        {
            if (stage == Game.Stages.Idle || stage == Game.Stages.Resolve)
                SetEnable(false);
        }
    }
}
