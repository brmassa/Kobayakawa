﻿using UnityEngine;
using UnityEngine.UI;

namespace Gamenific.Koba
{
    internal class UINextHumanPlayerDialog : MonoBehaviour
    {

        [SerializeField] private GameObject Panel = null;
        [SerializeField] private Button button = null;
        [SerializeField] private bool skip = false;

        private int humans = 0;

        #region MonoBehaviour
        private void Awake()
        {
            if (Panel == null)
                throw new UnityException("component needed");
            if (button == null)
                throw new UnityException("component needed");
        }

        private void OnEnable()
        {
            GameManager.Instance.OnGameStart += OnGameStart;
            GameManager.Instance.OnPlayerActionEnded += OnPlayerActionEnded;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnGameStart -= OnGameStart;
            GameManager.Instance.OnPlayerActionEnded -= OnPlayerActionEnded;
        }
        #endregion

        private void OnGameStart(Game game)
        {
            humans = 0;
            foreach (var player in game.Players)
            {
                if (player.Type == Player.Types.Human)
                    humans++;
            }
        }

        private void OnPlayerActionEnded(Player player)
        {
            if (humans >= 2 && !skip)
                Panel.SetActive(true);
            else
                Click();
        }

        public void Click()
        {
            Panel.SetActive(false);
            GameManager.Instance.NextPlayerAction(false);
        }
    }
}