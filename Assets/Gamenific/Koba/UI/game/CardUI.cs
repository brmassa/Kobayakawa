﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Gamenific.Koba
{
    internal class CardUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI m_Text = null;
        [SerializeField] private int m_CardValue = 12;

        public void Setup(int value = 0)
        {
            m_CardValue = value;
            m_Text.text = m_CardValue > 0 ? m_CardValue.ToString() : "";
        }
    }
}
