﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gamenific.Koba
{
    internal class CardsListUI : MonoBehaviour
    {
        [SerializeField] private CardUI m_CardPrefab = null;
        [SerializeField] private Transform m_CardListPlaceholder = null;

        private List<CardUI> m_Cards = new List<CardUI>();

        #region MonoBehaviour
        private void OnEnable()
        {
            GameManager.Instance.OnNewRound += Clear;
            GameManager.Instance.OnNewCardCurrent += NewCurrentCard;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnNewRound -= Clear;
            GameManager.Instance.OnNewCardCurrent -= NewCurrentCard;
        }
        #endregion MonoBehaviour

        private void Clear(int round)
        {
            foreach (var card in m_Cards)
                Destroy(card.gameObject);
            m_Cards.Clear();
        }

        private void NewCurrentCard(Card cardNew, Deck CardsInPlay)
        {
            var card = Instantiate(m_CardPrefab, m_CardListPlaceholder);
            card.Setup(cardNew.Value);
            m_Cards.Add(card);
            StartCoroutine(Reposition(card));
        }

        private IEnumerator Reposition(CardUI card)
        {
            yield return new WaitForEndOfFrame();
            card.transform.SetAsFirstSibling();
        }
    }
}
