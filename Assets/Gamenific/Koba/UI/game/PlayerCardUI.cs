﻿using UnityEngine;

namespace Gamenific.Koba
{
    internal class PlayerCardUI : MonoBehaviour
    {
        [SerializeField] private CardUI m_Card = null;
        [SerializeField] private Types type = Types.Card1;

        private enum Types
        {
            Card1,
            Card2
        }

        #region MonoBehaviour
        private void OnEnable()
        {
            GameManager.Instance.OnPlayerDrawStage += SetEnableCard;
            GameManager.Instance.OnPlayerBetStage += SetEnableCard;
            GameManager.Instance.OnPlayerSelectStage += SetEnableCard;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnPlayerDrawStage -= SetEnableCard;
            GameManager.Instance.OnPlayerBetStage -= SetEnableCard;
            GameManager.Instance.OnPlayerSelectStage -= SetEnableCard;
        }
        #endregion

        private void SetEnableCard(Player player)
        {
            var enable = player.Type == Player.Types.Human;
                if (type == Types.Card1)
                m_Card.Setup(enable && player.Card != null ? player.Card.Value : 0);
                else if (type == Types.Card2)
                m_Card.Setup(enable && player.Card2 != null ? player.Card2.Value : 0);
        }
    }
}
