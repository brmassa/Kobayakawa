using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Gamenific.Koba
{
    internal class UIPlayer : MonoBehaviour
    {
        [Header("Setup")]
        [SerializeField] private GameObject CurrentPlayerMarker = null;
        [SerializeField] private GameObject FirstPlayerMarker = null;
        [SerializeField] private GameObject EliminatedMarker = null;
        [SerializeField] private GameObject WeakestPlayer = null;
        [SerializeField] private GameObject RoundWinner = null;
        [SerializeField] private TextMeshProUGUI Tokens = null;
        [SerializeField] private TextMeshProUGUI Card = null;
        [SerializeField] private TextMeshProUGUI Card2 = null;
        [SerializeField] private TextMeshProUGUI CardDiscarted = null;
        [SerializeField] private GameObject DrawDecision = null;
        [SerializeField] private GameObject BuyDecision = null;
        [SerializeField] private GameObject PassDecision = null;
        [SerializeField] private GameObject BetDecision = null;
        [SerializeField] private TextMeshProUGUI FinalScore = null;

        [Header("Icon")]
        [SerializeField] private Image m_Icon = null;
        [SerializeField] private Sprite m_IconHuman = null;
        [SerializeField] private Sprite m_IconBot = null;

        [Header("")]
        [SerializeField] private bool debugMode = false;

        [Header("Data")]
        [SerializeField] private PlayerController player = null;

        private bool isCurrentPlayer = false;
        private bool weakestPlayer = false;

        #region MonoBehaviour
        private void OnEnable()
        {
            if (player == null)
                player = GetComponent<PlayerController>();

            if (player == null)
                throw new UnityException("component needed");
            if (Card == null)
                throw new UnityException("component needed");
            if (Card2 == null)
                throw new UnityException("component needed");
            if (Tokens == null)
                throw new UnityException("component needed");
            if (CurrentPlayerMarker == null)
                throw new UnityException("component needed");
            if (FirstPlayerMarker == null)
                throw new UnityException("component needed");
            if (DrawDecision == null)
                throw new UnityException("component needed");
            if (BuyDecision == null)
                throw new UnityException("component needed");
            if (BetDecision == null)
                throw new UnityException("component needed");
            if (PassDecision == null)
                throw new UnityException("component needed");
            if (EliminatedMarker == null)
                throw new UnityException("component needed");
            if (CardDiscarted == null)
                throw new UnityException("component needed");
            if (WeakestPlayer == null)
                throw new UnityException("component needed");
            if (RoundWinner == null)
                throw new UnityException("component needed");
            if (FinalScore == null)
                throw new UnityException("component needed");

            player.OnPlayerChanged += OnPlayerChanged;

            GameManager.Instance.OnNewRound += NewRound;
            GameManager.Instance.OnRoundPlayerWon += OnRoundPlayerWon;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnNewRound -= NewRound;
            GameManager.Instance.OnRoundPlayerWon -= OnRoundPlayerWon;
        }
        #endregion MonoBehaviour

        private void OnPlayerChanged(Player playerNew)
        {
            player.OnPlayerCurrent += NewCurrentPlayer;
            player.OnTokensChanged += ShowTokens;
            player.OnBeingFirstPlayer += ShowFirstPlayer;
            
            player.OnDrawChosen += _OnDrawChosen;
            player.OnGetCardChosen += OnGetCardChosen;

            player.OnCardGet += CardGet;
            player.OnEliminated += ShowPlayerEliminated;
            player.OnCardChosen += OnCardChosen;
            player.OnCardDiscarted += OnCardDiscarted;
            player.OnBetChosen += OnBetChosen;
            player.OnPassChosen += OnPassChosen;
            player.OnWeakestPlayer += ShowWeakestPlayer;
            player.OnWinRound += ShowRoundWinner;
            ResetAll();
        }

        private void OnBetChosen(int betTokens, int token)
        {
            BetDecision.SetActive(true);
        }

        private void OnPassChosen()
        {
            PassDecision.SetActive(true);
        }

        private void _OnDrawChosen()
        {
            DrawDecision.SetActive(true);
        }

        private void OnGetCardChosen()
        {
            BuyDecision.SetActive(true);
        }

        private void ShowTokens(int tokens)
        {
            Tokens.text = player.Tokens.ToString();
        }

        private void ShowCard(bool show = true, bool forceShow = false)
        {
            Card.gameObject.SetActive(show);
            if (debugMode || (player.CardSelected && player.Card != null))
            {
                Card.text = player.Card.Value.ToString();
            }

            //if ((isCurrentPlayer && ((player.Type == Player.Types.Human && m_ShowCardValues) || debugMode)) || forceShow)
            //    Card.text = player.Card != null ? player.Card.Value.ToString() : "-";
            //else
            //    Card.text = "";
        }

        private void ShowCard2(bool show)
        {
            Card2.gameObject.SetActive(show);
            if (player.CardSelected && player.Card2 != null)
            {
                Card2.text = player.Card2.Value.ToString();
            }

            //if (isCurrentPlayer || (player.CardSelected))
            //    Card2.text = player.Card2 != null ? player.Card2.Value.ToString() : "-";
            //else
            //    Card2.text = "";
        }

        private void CardGet(Card card)
        {
            ResetAll();
        }

        private void ShowCurrentPlayer(bool isCurrentPlayer)
        {
            CurrentPlayerMarker.gameObject.SetActive(isCurrentPlayer);
        }

        private void ShowFirstPlayer(bool firstPlayer)
        {
            FirstPlayerMarker.gameObject.SetActive(firstPlayer);
        }

        private void ShowPlayerEliminated()
        {
            EliminatedMarker.gameObject.SetActive(player.Eliminated);
        }

        private void ShowPlayerType()
        {
            if (player.Type == Koba.Player.Types.Bot)
            {
                m_Icon.sprite = m_IconBot;
            }
            else if (player.Type == Koba.Player.Types.Human)
            {
                m_Icon.sprite = m_IconHuman;
            }
        }

        private void OnCardChosen(Card card)
        {
            ResetAll();
        }

        private void OnCardDiscarted(Card card)
        {
            CardDiscarted.text = (player.CardDiscarted != null) ? player.CardDiscarted.Value.ToString() : "";
        }

        private void ShowFinalScore()
        {
            FinalScore.text = (player.Card.Value + (weakestPlayer ? GameManager.Instance.Game.CardCurrent.Value : 0)).ToString();
        }

        private void ShowWeakestPlayer(Card card)
        {
            WeakestPlayer.gameObject.SetActive(true);
        }

        private void ShowRoundWinner(int old, int newtokens)
        {
            RoundWinner.gameObject.SetActive(true);
        }

        private void ResetAll(bool roundEnd = false, bool forceShowCard = false)
        {
            ShowTokens(0);
            ShowCard(false, forceShowCard);
            ShowCard2(false);
            ShowFirstPlayer(player.FirstPlayer);
            ShowPlayerEliminated();
            ShowPlayerType();
            ShowCurrentPlayer(isCurrentPlayer && !roundEnd);
            OnCardDiscarted(null);
        }

        public void NewRound(int round)
        {
            weakestPlayer = false;
            WeakestPlayer.SetActive(false);
            RoundWinner.SetActive(false);
            BetDecision.SetActive(false);
            PassDecision.SetActive(false);
            DrawDecision.SetActive(false);
            BuyDecision.SetActive(false);
            FinalScore.text = "";
            ResetAll();
        }

        private void OnRoundPlayerWon(Player playerWinner, bool onlyBidder)
        {
            ResetAll(true, !onlyBidder && player.Bet);
        }

        private void NewCurrentPlayer(bool isCurrentPlayer)
        {
            if (this.isCurrentPlayer != isCurrentPlayer)
            {
                this.isCurrentPlayer = isCurrentPlayer;
                ResetAll();
            }
        }
    }
}
