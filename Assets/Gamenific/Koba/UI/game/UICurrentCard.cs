﻿using UnityEngine;
using TMPro;
using System.Collections;

namespace Gamenific.Koba
{
    public class UICurrentCard : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI text = null;

        #region MonoBehaviour
        private void OnValidate()
        {
            if (text == null)
                text = GetComponent<TextMeshProUGUI>();
            if (text == null)
                throw new UnityException("component needed");
        }

        private void OnEnable()
        {
            GameManager.Instance.OnNewCardCurrent += NewCurrentCard;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnNewCardCurrent -= NewCurrentCard;
        }
        #endregion MonoBehaviour

        private void NewCurrentCard(Card cardNew, Deck CardsInPlay)
        {
            text.text = "(" + cardNew.Value.ToString() + ")";

            bool firstCard = true;
            foreach (var card in CardsInPlay.CardsLoop())
            {
                if (firstCard)
                    firstCard = false;
                else
                    text.text += " - " + card.Value;
            }
        }
    }
}