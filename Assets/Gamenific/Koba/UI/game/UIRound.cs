﻿using UnityEngine;
using TMPro;

namespace Gamenific.Koba
{
    internal class UIRound : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI text = null;
        [SerializeField] private string m_FinalRoundText = " (FINAL ROUND)";

        #region MonoBehaviour
        private void OnValidate()
        {
            if (text == null)
                text = GetComponent<TextMeshProUGUI>();
            if (text == null)
                throw new UnityException("component needed");
        }

        private void Start()
        {
            GameManager.Instance.OnNewRound += Display;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnNewRound -= Display;
        }
        #endregion

        private void Display(int round)
        {
            text.text = round.ToString() + (round == GameManager.Instance.Game.RoundMax ? m_FinalRoundText : "");
        }
    }
}