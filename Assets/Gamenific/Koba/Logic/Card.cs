using UnityEngine;

namespace Gamenific.Koba
{
    [System.Serializable]
    internal class Card
    {
        [SerializeField]
        private int value;

        public int Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
    }
}
