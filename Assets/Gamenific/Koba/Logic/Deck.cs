using System.Collections.Generic;
using UnityEngine;

namespace Gamenific.Koba
{
    [System.Serializable]
    internal class Deck
    {
        #region Public
        public System.Action OnShuffled = null;

        public Card Draw()
        {
            if (Cards.Count > 0)
            {
                Card cardDraw = Cards[0];
                Cards.Remove(cardDraw);
                return cardDraw;
            }
            else
                throw new System.Exception("no more cards");
        }

        public void Init(List<Card> Cards, bool shuffle)
        {
            this.Cards = Cards;
            if (shuffle)
                Shuffle();
        }

        public int Count
        {
            get
            {
                if (Cards == null)
                    return 0;
                else
                    return Cards.Count;
            }
        }

        public void Add(Card cardNew)
        {
            // Fix a bug when the player have a blank card when the game begin
            if (cardNew.Value == 0)
                return;

            if (Cards == null)
                Cards = new List<Card>();
            Cards.Insert(0, cardNew);
        }

        public Card Peek { get { return Cards[0]; } }

        public IEnumerable<Card> CardsLoop()
        {
            foreach (var card in Cards)
            {
                yield return card;
            }
        }

        public void Shuffle()
        {
            List.Shuffle<Card>(Cards);

            if (OnShuffled != null)
                OnShuffled();
        }
        #endregion

        [SerializeField]
        private List<Card> Cards = new List<Card>();
    }
}
