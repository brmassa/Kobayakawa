﻿using System;

namespace Gamenific.Koba
{
    internal interface IGame
    {
        #region
        Action<int> OnNewRound { get; set; }
        Action<Game.Stages> OnStageChange { get; set; }
        Action<int> OnTokensInGameRound { get; set; }
        Action<int> OnTokensInReverse { get; set; }

        Action<Player> OnPlayerBet { get; set; }
        Action<Player> OnPlayerBetStage { get; set; }
        Action<Player> OnPlayerDrawStage { get; set; }
        Action<Player> OnPlayerSelectStage { get; set; }
        Action<Player> OnPlayerActionEnded { get; set; }
        Action<Player, int> OnPlayerCurrent { get; set; }
        Action<Player, int> OnPlayerFirst { get; set; }
        Action<Player, int> OnPlayerEliminated { get; set; }
        Action<Player, Card> OnPlayerCardChosen { get; set; }
        Action<Player, Card> OnPlayerCardDiscarted { get; set; }
        Action<Player, Card> OnPlayerWeakest { get; set; }

        Action<Player, bool> OnRoundPlayerWon { get; set; }
        Action OnRoundDraw { get; set; }

        Action<Game> OnGameStart { get; set; }
        Action<Player> OnGameWin { get; set; }
        Action<Card, Deck> OnNewCardCurrent { get; set; }
        #endregion

        void ChooseDraw();
        void ChooseGet();

        void ChooseCard(Card card);

        void ChooseBet(int tokens);
        void ChoosePass();

        int PlayerNum(Player player);

        void NextPlayerAction(bool reset);
    }
}
