﻿using System.Collections;

namespace Gamenific.Koba
{
    internal interface IPlayerDecider
    {
        IEnumerator DrawStageDecision();

        IEnumerator BetStageDecision();

        IEnumerator CardSelectStageDecision();
    }
}
