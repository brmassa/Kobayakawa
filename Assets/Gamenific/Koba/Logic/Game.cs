using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gamenific.Koba
{
    [System.Serializable]
    internal class Game : IGame
    {
        public enum Stages
        {
            Idle,
            Setup,
            Draw,
            Bet,
            Reveal,
            Resolve
        }

        private Stages stage = Stages.Idle;

        [SerializeField]
        private int round = 0;
        [SerializeField]
        private int roundMax = 7;
        [SerializeField]
        private int tokensReverve = 8;
        [SerializeField]
        private int tokensInGame = 0;
        [SerializeField]
        private List<Player> players = new List<Player>();
        [SerializeField]
        private int playerCurrent = 0;
        [SerializeField]
        private int playerFirst = 0;
        [SerializeField]
        private Deck cardsInPlay = new Deck();
        [SerializeField]
        private int playersPassed = 0;
        [SerializeField]
        private int lastWinner = -1;

        private delegate void NextPlayerActionDelegate(bool reset);

        private NextPlayerActionDelegate NextPlayerActionInternal;

        [SerializeField]
        private Deck deck = new Deck();

        #region Events
        public Action<int> OnNewRound { get; set; }
        public Action<Game.Stages> OnStageChange { get; set; }
        public Action<int> OnTokensInGameRound { get; set; }
        public Action<int> OnTokensInReverse { get; set; }

        public Action<Player> OnPlayerBet { get; set; }
        public Action<Player> OnPlayerBetStage { get; set; }
        public Action<Player> OnPlayerDrawStage { get; set; }
        public Action<Player> OnPlayerSelectStage { get; set; }
        public Action<Player> OnPlayerActionEnded { get; set; }
        public Action<Player, int> OnPlayerCurrent { get; set; }
        public Action<Player, int> OnPlayerFirst { get; set; }
        public Action<Player, int> OnPlayerEliminated { get; set; }
        public Action<Player, Card> OnPlayerCardChosen { get; set; }
        public Action<Player, Card> OnPlayerCardDiscarted { get; set; }
        public Action<Player, Card> OnPlayerWeakest { get; set; }

        public Action<Player, bool> OnRoundPlayerWon { get; set; }
        public Action OnRoundDraw { get; set; }

        public Action<Game> OnGameStart { get; set; }
        public Action<Player> OnGameWin { get; set; }
        public Action<Card, Deck> OnNewCardCurrent { get; set; }
        #endregion

        #region Public API

        [SerializeField]
        public Stages Stage
        {
            get { return stage; }
            protected set
            {
                stage = value;
                if (OnStageChange != null)
                    OnStageChange(stage);
            }
        }

        [SerializeField]
        public Card CardCurrent
        {
            get { return cardsInPlay.Count > 0 ? cardsInPlay.Peek : null; }
            protected set
            {
                if (value == null)
                    return;

                cardsInPlay.Add(value);

                if (OnNewCardCurrent != null)
                    OnNewCardCurrent(CardCurrent, cardsInPlay);
            }
        }

        [SerializeField]
        public int Round
        {
            get { return round; }
            protected set
            {
                round = value;
                if (OnNewRound != null)
                    OnNewRound(round);
            }
        }

        public int RoundMax { get { return roundMax; } }

        [SerializeField]
        public int PlayerFirst
        {
            get { return playerFirst; }
            set
            {
                if (players.Count > 0)
                {
                    if (playerFirst != value)
                    {
                        players[PlayerFirst].FirstPlayer = false;
                        playerFirst = value;
                    }
                    players[PlayerFirst].FirstPlayer = true;
                    if (OnPlayerFirst != null)
                        OnPlayerFirst(players[PlayerFirst], PlayerFirst);
                }
            }
        }

        [SerializeField]
        public int PlayerCurrent
        {
            get { return playerCurrent; }
            protected set
            {
                if (players.Count > 0)
                {
                    players[PlayerCurrent].CurrentPlayer = false;
                    playerCurrent = value;
                    players[PlayerCurrent].CurrentPlayer = true;
                    if (OnPlayerCurrent != null)
                        OnPlayerCurrent(players[PlayerCurrent], PlayerCurrent);
                }
            }
        }

        [SerializeField]
        public List<Player> Players
        {
            get { return players; }
        }

        [SerializeField]
        public int TokensInGame
        {
            get { return tokensInGame; }
            set
            {
                tokensInGame = value;
                if (OnTokensInGameRound != null)
                    OnTokensInGameRound(tokensInGame);
            }
        }

        [SerializeField]
        public int TokensInReserve
        {
            get { return tokensReverve; }
            set
            {
                tokensReverve = value;
                if (OnTokensInReverse != null)
                    OnTokensInReverse(tokensReverve);
            }
        }

        public Player PlayerAdd(Player playerNew)
        {
            players.Add(playerNew);
            return playerNew;
        }

        public Player PlayerRemove(Player playerNew)
        {
            players.Remove(playerNew);
            return playerNew;
        }

        public void PlayerBet(Player player)
        {
            if (players[PlayerCurrent] == player)
            {
                TokensInGame++;
                player.BetStage();
                if (OnPlayerBet != null)
                    OnPlayerBet(player);
            }
        }

        public Player PlayerWinnerCurrent()
        {
            Player playerWinner = null;
            int max = 0;
            foreach (var player in players)
            {
                if (player.Tokens > max)
                {
                    max = player.Tokens;
                    playerWinner = player;
                }
            }

            int current = PlayerFirst;
            while (true)
            {
                if (players[current].Bet)
                {
                }

                if (++current == players.Count)
                    current = 0;
                if (current == PlayerFirst)
                    break;
            }
            return playerWinner;
        }

        public void GameStart()
        {
            Stage = Stages.Setup;
            if (deck.Count == 0)
                GameNew();
            RoundNew();
        }

        public void RoundResolvePost()
        {
            if (CheckEndGameConditions())
                GameOver();
            else
                RoundNew();
        }

        #endregion

        #region IGame
        public void ChooseDraw()
        {
            CardDraw();
            NextPlayerActionInternal = NextPlayerDraw;
            PlayerActionEnded();
        }

        public void ChooseGet()
        {
            Card cardDrawn = null;
            cardDrawn = deck.Draw();
            players[PlayerCurrent].GetCard(cardDrawn);

            if (OnPlayerSelectStage != null)
                OnPlayerSelectStage(players[PlayerCurrent]);
        }

        public void ChooseCard(Card card)
        {
            if (OnPlayerCardChosen != null)
                OnPlayerCardChosen(players[PlayerCurrent], card);

            NextPlayerActionInternal = NextPlayerDraw;
            PlayerActionEnded();
        }

        public void ChooseBet(int tokens)
        {
            players[PlayerCurrent].Tokens -= tokens;
            TokensInGame += tokens;
            NextPlayerActionInternal = NextPlayerBet;
            PlayerActionEnded();
        }

        public void ChoosePass()
        {
            playersPassed++;
            NextPlayerActionInternal = NextPlayerBet;
            PlayerActionEnded();
        }

        public void PlayerActionEnded()
        {
            if (OnPlayerActionEnded != null)
                OnPlayerActionEnded(players[PlayerCurrent]);
        }

        public int PlayerNum(Player player)
        {
            return players.IndexOf(player);
        }

        public void NextPlayerAction(bool reset)
        {
            NextPlayerActionInternal(reset);
        }
        #endregion

        private void RoundNew()
        {
            Stage = Stages.Setup;
            Round++;

            // The first player is the winner of the previous round
            if (lastWinner >= 0)
                PlayerFirst = lastWinner;
            else
                PlayerFirst = UnityEngine.Random.Range(0, players.Count);

            // Add the main bet at the center
            TokensInGame = (TokensInReserve == 2) ? 2 : 1;
            TokensInReserve -= TokensInGame;

            // Retrieve cards from all players and the table
            CardClearPools();

            CardDraw();

            // Give a Card to each player
            Card cardDrawn;
            foreach (var player in players)
            {
                if (!player.Eliminated)
                {
                    player.Reset();

                    cardDrawn = deck.Draw();
                    player.GetCard(cardDrawn);
                }
            }

            NextPlayerDraw(true);
        }

        private void RoundResolve()
        {
            Stage = Stages.Resolve;

            int current = PlayerFirst;
            int maxCard = 0;
            Player maxPlayer = null;
            int minCard = 99;
            Player minPlayer = null;

            int playerBet = 0;
            foreach (var player in players)
            {
                if (player.Bet)
                    playerBet++;
            }

            // Find the lowest Card (doesnt matter the player order)
            if (playerBet > 1)
            {
                foreach (var player in players)
                {
                    if (!player.Eliminated && player.Bet)
                    {
                        if (player.Card.Value < minCard)
                        {
                            minCard = player.Card.Value;
                            minPlayer = player;
                        }
                    }
                }

                if (OnPlayerWeakest != null)
                    OnPlayerWeakest(minPlayer, CardCurrent);
                minPlayer.WeakestPlayer(CardCurrent);
            }

            // Now compute the best cards
            current = PlayerFirst;
            int currentCardValue;
            while (true)
            {
                if (!players[current].Eliminated && players[current].Bet)
                {
                    currentCardValue = players[current].Card.Value + (players[current] == minPlayer ? CardCurrent.Value : 0);
                    if (currentCardValue > maxCard)
                    {
                        maxCard = currentCardValue;
                        maxPlayer = players[current];
                    }
                }

                if (++current == players.Count)
                    current = 0;
                if (current == PlayerFirst)
                    break;
            }

            // Eliminate players
            current = PlayerFirst;
            if (maxPlayer != minPlayer)
            {
                while (true)
                {
                    if (players[current].Tokens + (players[current] == maxPlayer ? TokensInGame : 0) <= 0)
                    {
                        players[current].Eliminated = true;
                        if (OnPlayerEliminated != null)
                            OnPlayerEliminated(players[current], current);
                    }

                    if (++current == players.Count)
                        current = 0;
                    if (current == PlayerFirst)
                        break;
                }
            }

            lastWinner = players.IndexOf(maxPlayer);

            // Comunicate the round winner
            maxPlayer.WinTokens(TokensInGame);
            TokensInGame = 0;
            if (OnRoundPlayerWon != null)
                OnRoundPlayerWon(maxPlayer, minPlayer == null);
        }

        private bool CheckEndGameConditions()
        {
            return (TokensInReserve <= 0) || (round > roundMax) || (PlayersInGame == 1);
        }

        private int PlayersInGame
        {
            get
            {
                int playersInGame = 0;
                foreach (var player in players)
                {
                    if (!player.Eliminated)
                        playersInGame++;
                }
                return playersInGame;
            }
        }

        private void NextStage()
        {
            if (Stage == Stages.Draw)
                Stage = Stages.Bet;
            else if (Stage == Stages.Bet)
                Stage = Stages.Reveal;
            else if (Stage == Stages.Reveal)
                Stage = Stages.Resolve;
        }

        private void CardDraw()
        {
            // Reveal the top card
            Card cardDrawn = null;
            cardDrawn = deck.Draw();
            CardCurrent = cardDrawn;
        }

        private void CardClearPools()
        {
            Card card;
            while (cardsInPlay.Count > 0)
            {
                card = cardsInPlay.Draw();
                deck.Add(card);
            }

            foreach (var player in players)
            {
                if (player.Card != null)
                {
                    deck.Add(player.Card);
                    player.Card = null;
                }
                if (player.CardDiscarted != null)
                {
                    deck.Add(player.CardDiscarted);
                    player.CardDiscarted = null;
                }
            }

            deck.Shuffle();
        }

        private int NextPlayer(bool reset = false)
        {
            int newPlayer = reset ? PlayerFirst - 1 : PlayerCurrent;
            while (true)
            {
                if (++newPlayer >= players.Count)
                    newPlayer = 0;
                if (!players[newPlayer].Eliminated)
                    break;
            }
            return newPlayer;
        }

        private void NextPlayerDraw(bool reset = false)
        {
            Stage = Stages.Draw;
            var player = NextPlayer(reset);
            if (!reset && player == PlayerFirst)
            {
                NextPlayerActionInternal = NextPlayerBet;
                NextPlayerAction(true);
            }
            else
            {
                PlayerCurrent = player;
                players[PlayerCurrent].DrawStage();
                if (OnPlayerDrawStage != null)
                    OnPlayerDrawStage(players[PlayerCurrent]);
            }
        }

        private void NextPlayerBet(bool reset = false)
        {
            if (reset)
                playersPassed = 0;

            Stage = Stages.Bet;
            var player = NextPlayer(reset);
            if (!reset && player == PlayerFirst)
            {
                RoundResolve();
            }
            else
            {
                PlayerCurrent = player;
                players[PlayerCurrent].BetStage();

                if (OnPlayerBetStage != null)
                    OnPlayerBetStage(players[PlayerCurrent]);

                // If is the last player and no one yet bet, it altomatically wins the round
                if (playersPassed == PlayersInGame - 1)
                    players[PlayerCurrent].ChooseBet();
            }

        }

        private IEnumerable<int> NextPlayerLoop(bool reset = false)
        {
            int current;
            while (true)
            {
                current = NextPlayer(reset);
                if (current == PlayerFirst)
                    break;
                yield return current;
            }
        }

        private void GameNew()
        {
            List<Card> cards = new List<Card>();
            Card card;
            for (int cardValue = 1; cardValue <= 15; cardValue++)
            {
                card = new Card();
                card.Value = cardValue;
                cards.Add(card);
            }
            deck.Init(cards, true);

            lastWinner = -1;

            if (OnGameStart != null)
                OnGameStart(this);
        }

        private void GameOver()
        {
            Stage = Stages.Idle;
            if (OnGameWin != null)
                OnGameWin(PlayerWinnerCurrent());
        }
    }
}
