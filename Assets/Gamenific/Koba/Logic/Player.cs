using UnityEngine;
//using FullInspector;

namespace Gamenific.Koba
{
    [System.Serializable]
    internal class Player
    {
        [SerializeField]
        private Card card = null;
        [SerializeField]
        private Card card2 = null;
        [SerializeField]
        private Card discartedCard = null;
        [SerializeField]
        private int tokens = 0;
        [SerializeField]
        private Types type = Types.Human;
        [SerializeField]
        private bool eliminated = false;
        [SerializeField]
        private bool bet = false;
        [SerializeField]
        private bool cardSelected = false;
        [SerializeField]
        private bool firstPlayer = false;
        [SerializeField]
        private bool currentPlayer = false;
        private IGame game;

        public enum Types
        {
            Human,
            Bot,
            Remote
        }

        #region Public Signals
        public System.Action OnDrawStage;
        public System.Action OnBetStage;

        public System.Action OnDrawChosen = null;
        public System.Action OnGetCardChosen = null;
        public System.Action<int, int> OnBetChosen = null;
        public System.Action OnPassChosen = null;
        public System.Action<Card> OnCardChosen = null;
        public System.Action<Card> OnCardDiscarted = null;
        public System.Action<Card> OnCardGet = null;
        public System.Action<Card> OnWeakestPlayer = null;

        public System.Action<int, int> OnWinRound = null;
        public System.Action OnEliminated = null;
        public System.Action<bool> OnBeingFirstPlayer = null;
        public System.Action<bool> OnPlayerCurrent = null;

        public System.Action<int> OnTokensChanged = null;
        #endregion

        #region Public Properties
        [SerializeField]
        public Types Type
        {
            get { return type; }
            set { type = value; }
        }

        [SerializeField]
        public bool Bet
        {
            get { return bet; }
            protected set { bet = value; }
        }

        [SerializeField]
        public bool CardSelected
        {
            get { return cardSelected; }
            protected set { cardSelected = value; }
        }

        [SerializeField]
        public bool Eliminated
        {
            get { return eliminated; }
            set
            {
                eliminated = value;
                if (eliminated && OnEliminated != null)
                    OnEliminated();
            }
        }

        [SerializeField]
        public int Tokens
        {
            get { return tokens; }
            set
            {
                tokens = value;
                if (OnTokensChanged != null)
                    OnTokensChanged(tokens);
            }
        }

        [SerializeField]
        public Card Card { get { return card; } set { card = value; } }

        [SerializeField]
        public Card Card2 { get { return card2; } set { card2 = value; } }

        [SerializeField]
        public Card CardDiscarted { get { return discartedCard; } set { discartedCard = value; } }

        [SerializeField]
        public bool FirstPlayer
        {
            get { return firstPlayer; }
            set
            {
                firstPlayer = value;
                if (OnBeingFirstPlayer != null)
                    OnBeingFirstPlayer(firstPlayer);
            }
        }

        [SerializeField]
        public bool CurrentPlayer
        {
            get { return currentPlayer; }
            set
            {
                currentPlayer = value;
                if (OnPlayerCurrent != null)
                    OnPlayerCurrent(currentPlayer);
            }
        }

        public int PlayerNum
        {
            get { return game.PlayerNum(this); }
        }
        #endregion

        #region Public Stages
        public void DrawStage()
        {
            if (OnDrawStage != null)
                OnDrawStage();
        }

        public void BetStage()
        {
            if (OnBetStage != null)
                OnBetStage();
        }
        #endregion

        #region Public Decisions

        public void ChooseBet()
        {
            bet = true;
            int betTokens = 1;
            //tokens -= betTokens;
            if (OnBetChosen != null)
                OnBetChosen(betTokens, tokens);

            game.ChooseBet(betTokens);
        }

        public void ChoosePass()
        {
            if (OnPassChosen != null)
                OnPassChosen();
            game.ChoosePass();
        }

        public void ChooseCard()
        {
            CardDiscarted = Card2;
            Card2 = null;
            if (OnCardDiscarted != null)
                OnCardDiscarted(CardDiscarted);

            if (OnCardChosen != null)
                OnCardChosen(card);

            game.ChooseCard(card);
        }

        public void ChooseCard2()
        {
            SwapMainCard();
            ChooseCard();
        }

        public void ChooseDraw()
        {
            if (OnDrawChosen != null)
                OnDrawChosen();
            game.ChooseDraw();
        }

        public void ChooseGet()
        {
            if (OnGetCardChosen != null)
                OnGetCardChosen();
            game.ChooseGet();
        }
        #endregion

        #region Public Setup
        public void Init(IGame game, int tokens)
        {
            this.game = game;
            this.tokens = tokens;

            Eliminated = false;
            Bet = false;
            CardSelected = false;
            CardDiscarted = null;
        }

        public void WeakestPlayer(Card card)
        {
            if (OnWeakestPlayer != null)
                OnWeakestPlayer(card);
        }

        public Card Discart()
        {
            var cardOld = card;
            card = null;
            return cardOld;
        }

        public void Reset()
        {
            Bet = false;
            CardSelected = false;
        }

        public void GetCard(Card card)
        {
            if (this.card == null)
            {
                this.card = card;
                if (OnCardGet != null)
                    OnCardGet(card);
            }
            else
            {
                this.Card2 = card;
                if (OnCardGet != null)
                    OnCardGet(card);
            }
        }

        public void WinTokens(int newTokens)
        {
            tokens += newTokens;
            if (OnWinRound != null)
                OnWinRound(newTokens, tokens);
        }

        #endregion

        private void SwapMainCard()
        {
            Card cardBuffer = card;
            card = card2;
            card2 = cardBuffer;
        }
    }
}
