using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gamenific.Koba
{
    internal class GameManager : MonoBehaviour, IGame
    {
        private static GameManager instance;
        public static GameManager Instance
        {
            get { return instance; }
        }

        [SerializeField]
        private Game game = null;

        [SerializeField]
        private PlayerController playerPrefab = null;

        [SerializeField]
        private GameObject playerHolder = null;

        [SerializeField]
        private NewGame newGame = null;
        private List<PlayerController> players = new List<PlayerController>();

        #region Public Events
        public Action<int> OnNewRound { get; set; }
        public Action<Game.Stages> OnStageChange { get; set; }
        public Action<int> OnTokensInGameRound { get; set; }
        public Action<int> OnTokensInReverse { get; set; }

        public Action<Player> OnPlayerBet { get; set; }
        public Action<Player> OnPlayerBetStage { get; set; }
        public Action<Player> OnPlayerDrawStage { get; set; }
        public Action<Player> OnPlayerSelectStage { get; set; }
        public Action<Player> OnPlayerActionEnded { get; set; }
        public Action<Player, int> OnPlayerCurrent { get; set; }
        public Action<Player, int> OnPlayerFirst { get; set; }
        public Action<Player, int> OnPlayerEliminated { get; set; }
        public Action<Player, Card> OnPlayerCardChosen { get; set; }
        public Action<Player, Card> OnPlayerCardDiscarted { get; set; }
        public Action<Player, Card> OnPlayerWeakest { get; set; }

        public Action<Player, bool> OnRoundPlayerWon { get; set; }
        public Action OnRoundDraw { get; set; }

        public Action<Game> OnGameStart { get; set; }
        public Action<Player> OnGameWin { get; set; }
        public Action<Card, Deck> OnNewCardCurrent { get; set; }
        #endregion

        #region Private Events
        private void _OnNewRound(int round) { if (OnNewRound != null) OnNewRound(round); }

        private void _OnStageChange(Game.Stages stage) { if (OnStageChange != null) OnStageChange(stage); }

        private void _OnTokensInGameRound(int tokens) { if (OnTokensInGameRound != null) OnTokensInGameRound(tokens); }

        private void _OnTokensInReverse(int tokens) { if (OnTokensInReverse != null) OnTokensInReverse(tokens); }

        private void _OnPlayerBet(Player player) { if (OnPlayerBet != null) OnPlayerBet(player); }

        private void _OnPlayerBetStage(Player player) { if (OnPlayerBetStage != null) OnPlayerBetStage(player); }

        private void _OnPlayerDrawStage(Player player) { if (OnPlayerDrawStage != null) OnPlayerDrawStage(player); }

        private void _OnPlayerSelectStage(Player player) { if (OnPlayerSelectStage != null) OnPlayerSelectStage(player); }

        private void _OnPlayerActionEnded(Player player) { if (OnPlayerActionEnded != null) OnPlayerActionEnded(player); }

        private void _OnPlayerCurrent(Player player, int value) { if (OnPlayerCurrent != null) OnPlayerCurrent(player, value); }

        private void _OnPlayerFirst(Player player, int value) { if (OnPlayerFirst != null) OnPlayerFirst(player, value); }

        private void _OnPlayerEliminated(Player player, int value) { if (OnPlayerEliminated != null) OnPlayerEliminated(player, value); }

        private void _OnPlayerCardChosen(Player player, Card card) { if (OnPlayerCardChosen != null) OnPlayerCardChosen(player, card); }

        private void _OnPlayerCardDiscarted(Player player, Card card) { if (OnPlayerCardDiscarted != null) OnPlayerCardDiscarted(player, card); }

        private void _OnPlayerWeakest(Player player, Card card) { if (OnPlayerWeakest != null) OnPlayerWeakest(player, card); }

        private void _OnRoundPlayerWon(Player player, bool value) { if (OnRoundPlayerWon != null) OnRoundPlayerWon(player, value); }

        private void _OnRoundDraw() { if (OnRoundDraw != null) OnRoundDraw(); }

        private void _OnGameStart(Game game) { if (OnGameStart != null) OnGameStart(game); }

        private void _OnGameWin(Player player) { if (OnGameWin != null) OnGameWin(player); }

        private void _OnNewCardCurrent(Card card, Deck deck) { if (OnNewCardCurrent != null) OnNewCardCurrent(card, deck); }
        #endregion

        #region MonoBehaviour
        private void Awake()
        {
            if (instance == null)
                instance = this;
        }

        private void Start()
        {
            if (NewGameManager.Instance != null)
            {
                newGame = NewGameManager.Instance.game;
                SetupNewGame();
            }
        }

        private void OnDisable()
        {
            Unhook();
        }
        #endregion

        #region Public API
        public Game Game
        {
            get { return game; }
            protected set
            {
                if (game != null)
                    Unhook();

                game = value;
                Hook();
            }
        }

        [ContextMenu("SetupNewGame")]
        public void SetupNewGame()
        {
            // Destroy all existing players
            foreach (var player in players)
                Destroy(player.gameObject);
            players.Clear();

            // Generate a brand new game
            Game = new Game();

            playerHolder.SetActive(false);
            int order = 0;
            foreach (var newPlayerData in newGame.players)
            {
                ++order;

                var player = new Player();
                player.Type = newPlayerData.type;
                Game.PlayerAdd(player);
                player.Init(Game, newGame.tokensPerPlayer);

                var playerGO = (PlayerController)Instantiate(playerPrefab, playerHolder.transform);
                playerGO.Init(player, order);

                players.Add(playerGO);
            }
            playerHolder.SetActive(true);
            StartCoroutine(GameStart());
        }

        public void NextRound()
        {
            Game.RoundResolvePost();
        }
        #endregion

        private IEnumerator GameStart()
        {
            yield return new WaitForEndOfFrame();
            Game.GameStart();
        }

        #region IGame
        public void ChooseDraw() { Game.ChooseDraw(); }
        public void ChooseGet() { Game.ChooseGet(); }
        public void ChooseCard(Card card) { Game.ChooseCard(card); }
        public void ChooseBet(int tokens) { Game.ChooseBet(tokens); }
        public void ChoosePass() { Game.ChoosePass(); }
        public int PlayerNum(Player player) { return Game.PlayerNum(player); }
        public void NextPlayerAction(bool reset) { Game.NextPlayerAction(reset); }
        #endregion

        private void Hook()
        {
            Game.OnStageChange += _OnStageChange;

            Game.OnTokensInGameRound += _OnTokensInGameRound;
            Game.OnTokensInReverse += _OnTokensInReverse;

            Game.OnPlayerCurrent += _OnPlayerCurrent;
            Game.OnPlayerFirst += _OnPlayerFirst;
            Game.OnPlayerEliminated += _OnPlayerEliminated;
            Game.OnPlayerBet += _OnPlayerBet;
            Game.OnPlayerBetStage += _OnPlayerBetStage;
            Game.OnPlayerDrawStage += _OnPlayerDrawStage;
            Game.OnPlayerSelectStage += _OnPlayerSelectStage;
            Game.OnPlayerActionEnded += _OnPlayerActionEnded;
            Game.OnPlayerWeakest += _OnPlayerWeakest;

            Game.OnNewRound += _OnNewRound;
            Game.OnNewCardCurrent += _OnNewCardCurrent;
            Game.OnRoundPlayerWon += _OnRoundPlayerWon;

            Game.OnGameStart += _OnGameStart;
            Game.OnGameWin += _OnGameWin;

            Game.OnNewRound += _OnNewRound;
        }

        private void Unhook()
        {
            Game.OnStageChange -= _OnStageChange;

            Game.OnTokensInGameRound -= _OnTokensInGameRound;
            Game.OnTokensInReverse -= _OnTokensInReverse;

            Game.OnPlayerCurrent -= _OnPlayerCurrent;
            Game.OnPlayerFirst -= _OnPlayerFirst;
            Game.OnPlayerEliminated -= _OnPlayerEliminated;
            Game.OnPlayerBet -= _OnPlayerBet;
            Game.OnPlayerBetStage -= _OnPlayerBetStage;
            Game.OnPlayerDrawStage -= _OnPlayerDrawStage;
            Game.OnPlayerSelectStage -= _OnPlayerSelectStage;
            Game.OnPlayerActionEnded -= _OnPlayerActionEnded;
            Game.OnPlayerWeakest -= _OnPlayerWeakest;

            Game.OnNewRound -= _OnNewRound;
            Game.OnNewCardCurrent -= _OnNewCardCurrent;
            Game.OnRoundPlayerWon -= _OnRoundPlayerWon;

            Game.OnGameStart -= _OnGameStart;
            Game.OnGameWin -= _OnGameWin;
        }
    }
}
