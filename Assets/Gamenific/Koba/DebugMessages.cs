using UnityEngine;

namespace Gamenific.Koba
{
    public class DebugMessages : MonoBehaviour
    {
        #region MonoBehaviour
        private void OnEnable()
        {
            GameManager.Instance.OnNewRound += OnNewRound;
            GameManager.Instance.OnPlayerBet += OnPlayerBet;
            GameManager.Instance.OnRoundPlayerWon += OnPlayerWin;
            GameManager.Instance.OnPlayerCurrent += OnPlayerCurrent;
            GameManager.Instance.OnPlayerFirst -= OnPlayerFirst;
            GameManager.Instance.OnPlayerEliminated += OnPlayerEliminated;
            GameManager.Instance.OnNewCardCurrent += OnNewCurrentCard;
        }

        private void OnDisable()
        {
            GameManager.Instance.OnNewRound -= OnNewRound;
            GameManager.Instance.OnPlayerBet -= OnPlayerBet;
            GameManager.Instance.OnRoundPlayerWon -= OnPlayerWin;
            GameManager.Instance.OnPlayerCurrent -= OnPlayerCurrent;
            GameManager.Instance.OnPlayerFirst -= OnPlayerFirst;
            GameManager.Instance.OnPlayerEliminated -= OnPlayerEliminated;
            GameManager.Instance.OnNewCardCurrent -= OnNewCurrentCard;
        }
        #endregion

        private void OnNewRound(int round)
        {
            Debug.Log("New Round: " + round);
        }

        private void OnPlayerBet(Player player)
        {
            Debug.Log("Player to Bet/Pass: " + player.Card.Value);
        }

        private void OnPlayerWin(Player player, bool onlyBidder)
        {
            Debug.Log(string.Format("Player {0} wins with {1}", player.PlayerNum, player.Card.Value) + (onlyBidder ? " as only bidder" : ""));
        }

        private void OnPlayerCurrent(Player player, int playerIndex)
        {
            Debug.Log("Player current: " + (playerIndex + 1) + ". With the card: " + player.Card.Value);
        }

        private void OnPlayerFirst(Player player, int playerIndex)
        {
            Debug.Log("Player first: " + (playerIndex + 1));
        }

        private void OnPlayerEliminated(Player player, int playerIndex)
        {
            Debug.Log("Player eliminated: " + (playerIndex + 1));
        }

        private void OnNewCurrentCard(Card card, Deck cardsInPlay)
        {
            Debug.Log("Current card: " + card.Value);
        }
    }
}
